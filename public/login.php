<?php
/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

header('Content-Type: text/html; charset=UTF-8');
// Начинаем сессию.
session_start();
//Если пользователь запустил сайт впервые, то session_start() устанавливает cookie у клиента и создаёт временное хранилище на сервере, связанное с идентификатором пользователя.
//Определяет хранилище, связанное с переданным текущим идентификатором.
//Если в хранилище на сервере есть данные, они помещаются в массив $_SESSION.
if (!empty($_SESSION['login']))
{
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
    session_destroy();
    header('Location: index.php');
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>

    <head>
        <title>Авторизация</title>
    </head>
    <div class="Form">
        <form action="" method="post">
            Логин:<input name="login"/>
            Пароль:<input name="pass" type="password"/>
            <input type="submit" value="Войти" />
        </form>
    </div>


    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
    $user = 'u24033';
    $pass = '3245334';
    $db = new PDO('mysql:host=localhost;dbname=u24033', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $login = $_POST['login'];
    $stmt = $db->prepare("SELECT * FROM users1 WHERE login LIKE ?");
    $stmt->execute([$login]);
    $flag=0;
    while($row = $stmt->fetch())
    {
        if(!strcasecmp($row['login'],$_POST['login'])&&password_verify($_POST['pass'],$row['hash']))
        {
            $flag=1;
            $user_id=$row['user_id'];
        }
    }
    if($flag) {
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['uid'] = $user_id;
        header('Location: index.php');
    }
    else{
        header('Location: login.php');
    }
}
